# rPackV

A modpack originaly made for a private minecraft server

rPackV is a mod pack based on minecraft version 1.20.1 made for tweaking the vanilla minecraft experience that should run on almost every potato

![Intel ADL_GT2 Performance Showcase](https://gitlab.com/RGB__Toaster/rpackv/-/raw/main/images/ADL_GT2_Performance_Showcase.png)

As you can see above the modpack runs on my laptop with an Intel ADL GT2 with around 240FPS with 10 chunks render distance

<details>
<summary><h2>List of Included Mods</h2></summary>
<ul>
<li><a href="https://modrinth.com/mod/betterf3">BetterF3</a></li>
<li><a href="https://modrinth.com/plugin/chunky">Chunky</a></li>
<li><a href="https://modrinth.com/mod/elytra-slot">Elytra Slot</a></li>
<li><a href="https://modrinth.com/mod/gamma-utils">Gamma Utils</a></li>
<li><a href="https://modrinth.com/mod/iris">Iris Shaders</a></li>
<li><a href="https://modrinth.com/mod/jei">Just Enough Items (JEI)</a></li>
<li><a href="https://modrinth.com/mod/just-enough-professions-jep">Just Enough Professions (JEP)</a></li>
<li><a href="https://modrinth.com/mod/lithium">Lithium</a></li>
<li><a href="https://modrinth.com/mod/memoryleakfix">MemoryLeakFix</a></li>
<li><a href="https://modrinth.com/mod/modelfix">Model Gap Fix</a></li>
<li><a href="https://modrinth.com/mod/no-chat-reports">No Chat Reports</a></li>
<li><a href="https://modrinth.com/mod/screenshot-to-clipboard">Screenshot To Clipboard</a></li>
<li><a href="https://modrinth.com/plugin/simple-voice-chat">Simple Voice Chat</a></li>
<li><a href="https://modrinth.com/mod/sodium">Sodium</a></li>
<li><a href="https://modrinth.com/plugin/terra">Terra</a></li>
<li><a href="https://modrinth.com/mod/travelersbackpack">Traveler's Backpack</a></li>
<li><a href="https://modrinth.com/mod/vein-mining">Vein Mining</a></li>
<li><a href="https://modrinth.com/mod/lanterns-bow">Lanterns Belong on Walls</a></li>
<li><a href="https://modrinth.com/mod/trinkets">Trinkets</a></li>
<li><a href="https://modrinth.com/mod/dynamic-fps">Dynamic FPS</a></li>
<li><a href="https://modrinth.com/mod/journeymap">JourneyMap</a></li>
<li><a href="https://modrinth.com/mod/modmenu">Mod Menu</a></li>
<li><a href="https://modrinth.com/mod/starlight">Starlight</a></li>
<li><a href="https://modrinth.com/mod/antighost">AntiGhost</a></li>
<li><a href="https://modrinth.com/mod/dark-mode-everywhere-fabric">Dark Mode Everywhere</a></li>
<li><a href="https://modrinth.com/mod/eating-animation">Eating Animation</a></li>
<li><a href="https://modrinth.com/mod/lootr">Lootr</a></li>
<li><a href="https://modrinth.com/mod/decorative-blocks-fork">Decorative Blocks</a></li>
<li><a href="https://modrinth.com/mod/couplings">Couplings</a></li>
<li><a href="https://modrinth.com/mod/immersiveportals">Immersive Portals</a></li>
<li><a href="https://modrinth.com/mod/doors">More Doors</a></li>
<li><a href="https://modrinth.com/mod/badoptimizations">BadOptimizations</a></li>
<li><a href="https://modrinth.com/mod/anti-xray">AntiXray</a></li>
<li><a href="https://modrinth.com/mod/fast-ip-ping">fast-ip-ping</a></li>
<li><a href="https://modrinth.com/mod/voidtotem">Void Totem</a></li>
<li><a href="https://modrinth.com/mod/wi-zoom">WI Zoom</a></li>
<li><a href="https://modrinth.com/mod/no_fog">No Fog</a></li>
</ul>
</details>

<details>
<summary><h2>List of Included Texturepacks</h2></summary>
<ul>
<li><a href="https://modrinth.com/resourcepack/faithful-32x">Faithful 32x</a></li>
<li><a href="https://modrinth.com/resourcepack/fast-better-grass">Fast Better Grass</a></li>
</ul>

</details>

<details>
<summary><h2>List of Included Shaderpacks</h2></summary>
<ul>
<li><a href="https://modrinth.com/shader/complementary-unbound">Complementary Shaders - Unbound</a></li>
<li><a href="https://modrinth.com/shader/super-duper-vanilla">Super Duper Vanilla</a></li>
<li><a href="https://modrinth.com/shader/bsl-shaders">BSL Shaders</a></li>
</ul>

</details>

## License
This Project is Licensed under the <a href="https://www.gnu.org/licenses/gpl-3.0.en.html">GPL-V3 License</a>

